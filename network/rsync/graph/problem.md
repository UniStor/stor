# rsync slows down over time
sch: https://www.google.com/search?q=rsync+slows+down+over+time

[rsync is very slow (factor 8 to 10) compared to cp on copying files from nfs-share to local dir](https://unix.stackexchange.com/questions/91382/rsync-is-very-slow-factor-8-to-10-compared-to-cp-on-copying-files-from-nfs-sha)

# Solution!
guide: [A look at rsync performance](https://lwn.net/Articles/400489/)
