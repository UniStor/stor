ip=10.0.0.2

rsync rsync://$ip

test(){
  # https://www.atlantic.net/vps-hosting/how-to-setup-rsync-daemon-linux-server/
  rsync -rdt rsync://$ip/
}
