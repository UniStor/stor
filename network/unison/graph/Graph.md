https://www.cis.upenn.edu/~bcpierce/unison/

# ssh username
https://unix.stackexchange.com/questions/280302/unison-login-over-ssh-with-different-username-at-remote-server

# Guide
https://www.digitalocean.com/community/tutorials/how-to-back-up-large-directories-with-unison-on-ubuntu-16-04

# Wiki
https://wiki.archlinux.org/index.php/Unison
